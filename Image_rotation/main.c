#include <stdio.h>
#include <stdint.h>
#include "rotator.h"
#include "bmp.h"
#include "io.h"
#include "util.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header Input_name.bmp Output_name.bmp\n");
}

int main(int argc, char** argv) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n");
    //if (argc > 3) err("Too many arguments \n" );

    struct Image img = {0};
    fprintf(stderr,"File reading finished with exit code %d\n", fromFile(argv[1], &img));

    struct Image newImg = rotate(img);
   
    fprintf(stderr,"File writing finished with exit code %d\n", toFile(argv[2], &newImg));

    return 0;
}