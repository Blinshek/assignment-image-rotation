cmake_minimum_required(VERSION 3.17)
project(image_rotation)

set(CMAKE_CXX_STANDARD 14)

add_executable(image_rotation main.c util.h util.c bmp.c bmp.h io.c io.h rotator.c rotator.h)
