#ifndef BMP_H
#define BMP_H

#define PADDING_SIZE 4

enum ReadStatus fromBmp(FILE* in, struct Image* img);
enum WriteStatus toBmp(FILE* out, struct Image const* img);

#endif