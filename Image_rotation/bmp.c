#include <stdio.h>
#include <stdint.h>
#include <malloc.h>
#include "rotator.h"
#include "io.h"
#include "bmp.h"

#pragma pack(1)
struct BmpHeader {
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

const uint32_t MB = 0x4D42;

struct BmpHeader  makeHeader(struct Image const* IMG) {
	struct BmpHeader header = (struct BmpHeader){
			.bfType = MB, //Код bmp файла
			.bfileSize = IMG->width * IMG->height * sizeof(struct Pixel) + IMG->width % 4 * IMG->height + sizeof(struct BmpHeader),
			.bfReserved = 0,
			.bOffBits = sizeof(struct BmpHeader),
			.biSize = 40,
			.biWidth = IMG->width,
			.biHeight = IMG->height,
			.biPlanes = 1,
			.biBitCount = 24,
			.biCompression = 0,
			.biSizeImage = 0,
			.biXPelsPerMeter = 0,
			.biYPelsPerMeter = 0,
			.biClrUsed = 0,
			.biClrImportant = 0,
	};
	return header;
}

uint32_t getPadding(const uint64_t PIC_WIDTH) {
	size_t widthSize = PIC_WIDTH * sizeof(struct Pixel);
	if (widthSize % PADDING_SIZE == 0) return 0;
	return PADDING_SIZE - (widthSize % PADDING_SIZE);
}


/*  serializer   */
enum WriteStatus toBmp(FILE* out, struct Image const* IMG) {
	struct BmpHeader header = makeHeader(IMG);

	uint32_t headerSize = sizeof(struct BmpHeader);
	uint32_t pixelSize = sizeof(struct Pixel);

	if (fwrite(&header, headerSize, 1, out) < 1) return WRITE_ERROR;

	char paddingBytes[3] = { 0 };
	uint32_t padding = getPadding(IMG->width);

	for (size_t i = 0; i < IMG->height; ++i)
		if ((fwrite(IMG->data + i * IMG->width, pixelSize, IMG->width, out) != IMG->width) ||
			(fwrite(paddingBytes, padding, 1, out) != 1 && padding != 0) ||
			(fflush(out) != 0)) return WRITE_ERROR;


	if (fseek(out, 0, SEEK_SET) != 0) return WRITE_ERROR;
	return WRITE_OK;
}

enum ReadStatus fromBmp(FILE* in, struct Image* img) {
	if (!in) return READ_NO_SUCH_FILE;

	struct BmpHeader header = { 0 };
	const uint32_t pixelSize = sizeof(struct Pixel);
	enum ReadStatus readStatus = { 0 };

	if (fread_s(&header, sizeof(struct BmpHeader), sizeof(struct BmpHeader), 1, in) != 1) {
		readStatus = READ_INVALID_HEADER;
	}

	if (header.bfType != MB) {
		readStatus = READ_INVALID_SIGNATURE;
	}

	if (header.bOffBits != sizeof(struct BmpHeader)) {
		readStatus = READ_INVALID_BITS;
	}

	img->width = header.biWidth;
	img->height = header.biHeight;
	img->data = malloc(img->width * img->height * pixelSize);

	struct Pixel* data = malloc(sizeof(struct Pixel) * img->width * img->height);
	const uint32_t padding = getPadding(img->width);

	for (uint64_t i = 0; i < img->height; i++) {
		//if (fread(&(data[i * img->width]), sizeof(struct Pixel), img->width, in) < pixelSize) return READ_INVALID_BITS;
		if ((fread(img->data + i * img->width, pixelSize, img->width, in) < pixelSize) ||
			(fseek(in, padding, SEEK_CUR) != 0))
			readStatus = READ_INVALID_BITS;
	}

	//img->data = data;
	if (readStatus != READ_OK)
		free(img->data);
	return readStatus;
}