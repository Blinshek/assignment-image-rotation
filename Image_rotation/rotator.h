#ifndef ROTATOR_H
#define ROTATOR_H

struct Pixel { uint8_t b, g, r; };

struct Image {
    uint64_t width, height;
    struct Pixel* data;
};

struct Image rotate( struct Image const source );

#endif