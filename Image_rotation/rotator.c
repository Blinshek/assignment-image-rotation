#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <malloc.h>
#include "rotator.h"

static uint64_t getPixelAddress(const struct Image* const img, uint64_t col, uint64_t row) {
    return col * img->height + img->height - row - 1;
}

struct Image rotate( struct Image const img ){
    struct Image outputImg = {0};
    outputImg.height = img.width;
    outputImg.width = img.height;
    outputImg.data = (struct Pixel*)malloc(img.height * img.width * sizeof(struct Pixel));

    for (uint64_t  row = 0; row < img.height; row++)
        for (uint64_t col = 0; col < img.width; col++) {
            //outputImg.data[row + ((outputImg.height - col - 1) * outputImg.width)] = img.data[col + row * img.width];
            outputImg.data[getPixelAddress(&img, col, row)] = img.data[row * img.width + col];
        }
    return outputImg;
}