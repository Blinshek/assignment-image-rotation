#ifndef IO_H
#define IO_H

enum ReadStatus {
    READ_OK = 0,
    READ_NO_SUCH_FILE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_CANNOT_OPEN_FILE,
    READ_INVALID_SIGNATURE,
    READ_UNEXPECTED_END_OF_FILE
    /* ���� ������ ������  */
};
enum  WriteStatus {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_BITS
    /* ���� ������ ������  */
};

enum ReadStatus fromFile(const char* name, struct Image const* img);
enum WriteStatus toFile(const char* name, struct Image const* img);

#endif