#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include "io.h"
#include "bmp.h"

enum ReadStatus fromFile(const char* name, struct Image const* img) {
    FILE* input = NULL;
    if (fopen_s(&input, name, "rb")) return READ_CANNOT_OPEN_FILE;
    enum ReadStatus result = fromBmp(input, img);
    fclose(input);
    return result;
}

enum WriteStatus toFile(const char* name, struct Image const* img) {
    FILE* output = NULL;
    if (fopen_s(&output, name, "wb")) return READ_CANNOT_OPEN_FILE;
    enum ReadStatus result = toBmp(output, img);
    fclose(output);
    return result;
}